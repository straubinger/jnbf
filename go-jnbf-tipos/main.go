package main

import (
	"flag"
	"os"
	"path/filepath"

	"jnbf/formato"

	"github.com/esavara/typescriptify-golang-structs/typescriptify"
	"github.com/sirupsen/logrus"
)

func main() {
	ubicacionPtr := flag.String("salida", "", "archivo ts donde iran los tipos generados")

	flag.Parse()

	archivo, err := filepath.Abs(*ubicacionPtr)
	if err != nil {
		logrus.Panicf("no se pudo obtener ubicacion absoluta: %v", err)
	}

	converter := typescriptify.New()
	converter.Add(formato.Coleccion{})
	converter.AddEnum(formato.TipoContenido)

	converter.BackupDir = ""
	converter.CreateInterface = true
	err = converter.ConvertToFile(archivo)
	if err != nil {
		logrus.Errorf("no se pudo convertir structs a tipos typescript: %v", err)
		os.Exit(1)
	}

}
