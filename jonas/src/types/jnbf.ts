/* Do not change, this code is generated from Golang structs */

export enum ContenidoTipo {
  Parrafo = 0,
  Prosa = 1,
  Capitulo = 2,
  Titulo1 = 3,
  Titulo2 = 4,
  Titulo3 = 5,
  Titulo4 = 6,
  Titulo5 = 7,
  Titulo6 = 8,
  Seccion1 = 9,
  Seccion2 = 10,
  Seccion3 = 11,
  Seccion4 = 12,
  Seccion5 = 13,
  Seccion6 = 14,
  SubSeccion1 = 15,
  SubSeccion2 = 16,
  SubSeccion3 = 17,
  SubSeccion4 = 18,
  SubSeccion5 = 19,
  SubSeccion6 = 20,
  SubSubSeccion1 = 21,
  SubSubSeccion2 = 22,
  SubSubSeccion3 = 23,
  SubSubSeccion4 = 24,
  SubSubSeccion5 = 25,
  SubSubSeccion6 = 26,
  SubSubSubSeccion1 = 27,
  SubSubSubSeccion2 = 28,
  SubSubSubSeccion3 = 29,
  SubSubSubSeccion4 = 30,
  SubSubSubSeccion5 = 31,
  SubSubSubSeccion6 = 32,
  NotaAlPie = 33,
  NotaFinal = 34,
}
export interface Contenido {
  id: number
  contenedor_id: number
  contenido_id?: number
  marca_en_padre?: number
  tipo: ContenidoTipo
  texto: number[]
  hijos: Contenido[]
}
export interface Contenedor {
  id: number
  libro_id: number
  contenidos: Contenido[]
}
export interface Libro {
  id: number
  coleccion_id: number
  contenedores: Contenedor[]
  volumen?: number
  nombre?: string
  nomenclaturas: string[]
}
export interface Revisor {
  id: number
  coleccion_id: number
  titulo: string
  nombre: string
  nombre_segundo: string
  apellido_primero: string
  apellido_segundo: string
  fecha: Date
}
export interface Coleccion {
  id: number
  titulo: string
  subtitulo?: string
  autor?: string
  imprimatur?: Revisor
  nihil_obstat?: Revisor
  revision?: number
  libros: Libro[]
  idiomas: string[]
}
