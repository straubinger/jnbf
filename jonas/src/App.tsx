import { hot } from 'react-hot-loader'
import React from 'react'

const App = () => <div>Hola mundo desde React!</div>

export default hot(module)(App)
