module jnbf

go 1.14

require (
	github.com/esavara/typescriptify-golang-structs v0.0.12
	github.com/sirupsen/logrus v1.7.0
	github.com/tkrajina/typescriptify-golang-structs v0.1.3 // indirect
)
