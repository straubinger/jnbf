package formato

import "time"

// Contenido representa dato textual con un tipo y opciones que tienen sentido
// o no segun el tipo del contenido, esta estructura puede albergar hijos del
// tipo Contenido. El campo MarcaEnPadre indica una demarcacion en determinada
// posicion dentro del texto del Contenido padre, util para cosas como notas al
// pie.
type Contenido struct {
	ID           uint64        `yaml:"id" json:"id"`
	ContenedorID uint64        `yaml:"contenedor_id" json:"contenedor_id"`
	ContenidoID  *uint64       `yaml:"contenido_id" json:"contenido_id"`
	MarcaEnPadre *uint64       `yaml:"marca_en_padre" json:"marca_en_padre"`
	Tipo         ContenidoTipo `yaml:"tipo" json:"tipo"`
	Texto        []byte        `yaml:"texto" json:"texto"`
	Hijos        []*Contenido  `yaml:"hijos" json:"hijos"`
}

// Contenedor mantiene una lista de contenidos de distintos tipos
type Contenedor struct {
	ID         uint64      `yaml:"id" json:"id"`
	LibroID    uint64      `yaml:"libro_id" json:"libro_id"`
	Contenidos []Contenido `yaml:"contenidos" json:"contenidos"`
}

// Libro representa un libro, tiene una coleccion de contenedores que
// representan grupos de contenidos, un libro puede ser puesto en un volumen
// determinado por un numero entero.
type Libro struct {
	ID            uint64       `yaml:"id" json:"id"`
	ColeccionID   uint64       `yaml:"coleccion_id" json:"coleccion_id"`
	Contenedores  []Contenedor `yaml:"contenedores" json:"contenedores"`
	Volumen       *int64       `yaml:"volumen" json:"volumen"`
	Nombre        *string      `yaml:"nombre" json:"nombre"`
	Nomenclaturas []string     `yaml:"nomenclaturas" json:"nomenclaturas"`
}

// Revisor es una persona que ha hecho algo de autoridad con la coleccion de
// libros, esta estructura se usa para los campos de Imprimatur y Nihil Obstat
type Revisor struct {
	ID              uint64    `yaml:"id" json:"id"`
	ColeccionID     uint64    `yaml:"coleccion_id" json:"coleccion_id"`
	Titulo          string    `yaml:"titulo" json:"titulo"`
	Nombre          string    `yaml:"nombre" json:"nombre"`
	NombreSegundo   string    `yaml:"nombre_segundo" json:"nombre_segundo"`
	ApellidoPrimero string    `yaml:"apellido_primero" json:"apellido_primero"`
	ApellidoSegundo string    `yaml:"apellido_segundo" json:"apellido_segundo"`
	Fecha           time.Time `yaml:"fecha" json:"fecha" ts_type:"Date"`
}

// Coleccion es una coleccion de libros, aqui se establece algunos datos sobre
// la coleccion como titulo, algun subtitulo, autor, etc.
type Coleccion struct {
	ID          uint64   `yaml:"id" json:"id"`
	Titulo      string   `yaml:"titulo" json:"titulo"`
	Subtitulo   *string  `yaml:"subtitulo" json:"subtitulo"`
	Autor       *string  `yaml:"autor" json:"autor"`
	Imprimatur  *Revisor `yaml:"imprimatur" json:"imprimatur"`
	NihilObstat *Revisor `yaml:"nihil_obstat" json:"nihil_obstat"`
	Revision    *int64   `yaml:"revision" json:"revision"`
	Libros      []Libro  `yaml:"libros" json:"libros"`
	Idiomas     []string `yaml:"idiomas" json:"idiomas"`
}
