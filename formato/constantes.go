package formato

type ContenidoTipo int

const (
	Parrafo ContenidoTipo = iota
	Prosa
	Capitulo
	Titulo1
	Titulo2
	Titulo3
	Titulo4
	Titulo5
	Titulo6
	Seccion1
	Seccion2
	Seccion3
	Seccion4
	Seccion5
	Seccion6
	SubSeccion1
	SubSeccion2
	SubSeccion3
	SubSeccion4
	SubSeccion5
	SubSeccion6
	SubSubSeccion1
	SubSubSeccion2
	SubSubSeccion3
	SubSubSeccion4
	SubSubSeccion5
	SubSubSeccion6
	SubSubSubSeccion1
	SubSubSubSeccion2
	SubSubSubSeccion3
	SubSubSubSeccion4
	SubSubSubSeccion5
	SubSubSubSeccion6
	NotaAlPie
	NotaFinal
)

var TipoContenido = []ContenidoTipo{
	Parrafo,
	Prosa,
	Capitulo,
	Titulo1,
	Titulo2,
	Titulo3,
	Titulo4,
	Titulo5,
	Titulo6,
	Seccion1,
	Seccion2,
	Seccion3,
	Seccion4,
	Seccion5,
	Seccion6,
	SubSeccion1,
	SubSeccion2,
	SubSeccion3,
	SubSeccion4,
	SubSeccion5,
	SubSeccion6,
	SubSubSeccion1,
	SubSubSeccion2,
	SubSubSeccion3,
	SubSubSeccion4,
	SubSubSeccion5,
	SubSubSeccion6,
	SubSubSubSeccion1,
	SubSubSubSeccion2,
	SubSubSubSeccion3,
	SubSubSubSeccion4,
	SubSubSubSeccion5,
	SubSubSubSeccion6,
	NotaAlPie,
	NotaFinal,
}

func (t ContenidoTipo) TSName() string {
	switch t {
	case Parrafo:
		return "Parrafo"
	case Prosa:
		return "Prosa"
	case Capitulo:
		return "Capitulo"
	case Titulo1:
		return "Titulo1"
	case Titulo2:
		return "Titulo2"
	case Titulo3:
		return "Titulo3"
	case Titulo4:
		return "Titulo4"
	case Titulo5:
		return "Titulo5"
	case Titulo6:
		return "Titulo6"
	case Seccion1:
		return "Seccion1"
	case Seccion2:
		return "Seccion2"
	case Seccion3:
		return "Seccion3"
	case Seccion4:
		return "Seccion4"
	case Seccion5:
		return "Seccion5"
	case Seccion6:
		return "Seccion6"
	case SubSeccion1:
		return "SubSeccion1"
	case SubSeccion2:
		return "SubSeccion2"
	case SubSeccion3:
		return "SubSeccion3"
	case SubSeccion4:
		return "SubSeccion4"
	case SubSeccion5:
		return "SubSeccion5"
	case SubSeccion6:
		return "SubSeccion6"
	case SubSubSeccion1:
		return "SubSubSeccion1"
	case SubSubSeccion2:
		return "SubSubSeccion2"
	case SubSubSeccion3:
		return "SubSubSeccion3"
	case SubSubSeccion4:
		return "SubSubSeccion4"
	case SubSubSeccion5:
		return "SubSubSeccion5"
	case SubSubSeccion6:
		return "SubSubSeccion6"
	case SubSubSubSeccion1:
		return "SubSubSubSeccion1"
	case SubSubSubSeccion2:
		return "SubSubSubSeccion2"
	case SubSubSubSeccion3:
		return "SubSubSubSeccion3"
	case SubSubSubSeccion4:
		return "SubSubSubSeccion4"
	case SubSubSubSeccion5:
		return "SubSubSubSeccion5"
	case SubSubSubSeccion6:
		return "SubSubSubSeccion6"
	case NotaAlPie:
		return "NotaAlPie"
	case NotaFinal:
		return "NotaFinal"
	default:
		return "??"
	}
}
